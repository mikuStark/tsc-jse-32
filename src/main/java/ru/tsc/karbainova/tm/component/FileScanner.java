package ru.tsc.karbainova.tm.component;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import lombok.NonNull;
import ru.tsc.karbainova.tm.command.AbstractCommand;

import java.util.Map;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

public class FileScanner {
    @NonNull
    private static final String PATH = "./";
    private static final int interval = 1;
    private final ScheduledExecutorService es = Executors.newSingleThreadScheduledExecutor();
    private final List<String> commands = new ArrayList<>();
    private final Bootstrap bootstrap;

    public FileScanner(Bootstrap bootstrap) {
        this.bootstrap = bootstrap;
    }

    public void init() {
        commands.addAll(
                bootstrap.getCommandService().getArguments().entrySet().stream()
                .map(Map.Entry::getKey)
                .collect(Collectors.toList())
        );
        es.scheduleWithFixedDelay(this::run, 0, interval, TimeUnit.SECONDS);
    }

    public void run() {
        @NonNull final File file = new File(PATH);
        Arrays.stream(file.listFiles())
                .filter(o -> o.isFile() && commands.contains(o.getName()))
                .forEach(o -> {
                    bootstrap.executeCommandByArg(o.getName());
                    o.delete();
                });
    }
}
